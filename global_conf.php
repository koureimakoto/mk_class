<?php
/*
 *  Global Path
 */
define("__COMMONS_ERRORS__", "commons/errors/error.class.php");
define("__PROCEDURES__"    , "src/procedures_c.php"      );


/*
 *  Set DevMode and Log
 */
define("DEVMODE"    , TRUE);
define("LOG"        , TRUE);
define("__LOG_DIR__", "");
/*
 *  Locate Setting for GetText
 */
if(extension_loaded("gettext") === FALSE)
   exit("Extensio GetText Error or unistalled");

define("LOCALE"        , "pt-BR.utf8");
define("DOMAIN"        , "main");
define("__LOCALE_DIR__", "/var/www/html/mk_class/translation");


putenv("LANG="     . LOCALE);
putenv("LANGUAGE=" . LOCALE);

setlocale(LC_ALL, "pt-BR.utf8");

bindtextdomain( DOMAIN, __LOCALE_DIR__ );

textdomain(DOMAIN);





?>
