/*COMANDOS DE TESTE*/
SHOW TIMEZONE;

CREATE EXTENSION IF NOT EXISTS ltree;
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";



CREATE SCHEMA IF NOT EXISTS teacher;
CREATE SCHEMA IF NOT EXISTS apprendice;
CREATE SCHEMA IF NOT EXISTS global;


CREATE TABLESPACE ts_user_partitions
OWNER CURRENT_USER
LOCATION 'lib/postgresql/11/main/mk_appdata/ts_user_partition';

CREATE TABLESPACE ts_default
OWNER CURRENT_USER
LOCATION 'lib/postgresql/11/main/mk_appdata/ts_app_default';




-- skill
-- avatar_skill
-- type      [verified]
-- avatar (teacher - apprendice)
-- purchased_assets
-- assets    
-- prefix         [removed]
-- user           [verified]
-- register       [verified]
-- auth (points)  [verified]




-- Table log to store table modifitcation data
 
CREATE TABLE IF NOT EXISTS log_modification(
   created TIMESTAMP WITH TIME ZONE
,  updated TIMESTAMP WITH TIME ZONE
,  removed TIMESTAMP WITH TIME ZONE
);


CREATE TABLE IF NOT EXISTS registers(
   id          SERIAL      PRIMARY KEY

,  status      SMALLINT    DEFAULT 0         -- 0 = sent_email, 1 = error_email, 2 = confirmed, 3 = removed
,  acc_hash    VARCHAR(64) NOT     NULL      -- account_hash to confirm registered email
,  send_email  TIMESTAMP   WITH    TIME ZONE
,  acc_created TIMESTAMP   WITH    TIME ZONE
,  acc_removed TIMESTAMP   WITH    TIME ZONE
  
,  UNIQUE(acc_hash)
);


-- Login | Logout authtication

CREATE TABLE IF NOT EXISTS auth(
   id               SERIAL               PRIMARY KEY
,  regs_id          INTEGER              NOT     NULL 

,  email_updated    TIMESTAMP            WITH    TIME ZONE
,  psswd_updated    TIMESTAMP            WITH    TIME ZONE  
,  email            VARCHAR(60) ARRAY[3] DEFAULT '{}'
,  passwd_hash      VARCHAR(64) ARRAY[3] DEFAULT '{}'

--,  failed_attempts  SMALLINT    DEFAULT 0 
--,  old_punishment   SMALLINT    DEFAULT 0   -- sum of all old punishment and if exceeds 20 account will be banned
--,  cur_punishment   SMALLINT    DEFAULT 0   -- 0 = no punishment | locked login | banned
--,  unlocks_in       TIMESTAMP   WITH TIME ZONE

,  UNIQUE(regs_id, email)
,  CONSTRAINT fk_regs FOREIGN KEY (regs_id) REFERENCES registers(id) 
);


-- Global User

CREATE TABLE IF NOT EXISTS users(
   id        SERIAL      PRIMARY KEY
,  auth_id   INTEGER     NOT     NULL  -- FK

,  prefix    VARCHAR(20) DEFAULT ''                                           
,  name      VARCHAR(30) NOT     NULL CHECK (name <> '')
,  last      VARCHAR(30) NOT     NULL CHECK (name <> '')
,  middle    VARCHAR(50) DEFAULT ''
,  gender    SMALLINT    NOT     NULL  -- GENDER: -2 = male |  -1 = female  | 0 = other | 1 ... [feature]      
,  birth     DATE        NOT     NULL

,  UNIQUE(auth_id),
,  CONSTRAINT fk_auth FOREIGN KEY (auth_id)   REFERENCES auth(id)
)  INHERITS (log_modification);


CREATE TABLE IF NOT EXISTS business(
   id           SERIAL       PRIMARY KEY
,  auth         BOOLEAN      -- TRUE : real businesss  -  FALSE :     
,  company_name VARCHAR(100) NOT NULL
,  trading_name VARCHAR(100) NOT NULL
,  facebook     VARCHAR(50)
,  instagram    VARCHAR(50)
,  twitter      VARCHAR(50)
,  youtube      VARCHAR(80)
);

CREATE TABLE IF NOT EXISTS brazil_business(
   CNPJ     VARCHAR(14) DEFAULT ''
) INHERITS (business);


CREATE TABLE IF NOT EXISTS member_detail(
   id SERIAL PRIMARY KEY
) INHERITS (log_modification, cron);




-- partition by member and bus_id. 

CREATE TABLE IF NOT EXISTS business_team(
   id         SERIAL
,  member_id  INTEGER   NOT NULL
,  bus_id     INTEGER   NOT NULL
,  function   SMALLINT  NOT NULL -- LEVEL: -2 = sys admin / -1 = school admin / 0 = member 
,  sig        UUID      DEFAULT uuid_generate_v4 ()

,  CONSTRAINT fk_member   FOREIGN KEY (member_id)   REFERENCES users(id)
,  CONSTRAINT fk_business FOREIGN KEY (bus_id)      REFERENCES business(id)
)   INHERITS (log_modification, cron);




-- select ... from p_game_team_sig wheere function = 'func';
-- Case the business close in the future, add cronjob to drop partition e truncate data.
-- CREATE TABLE IF NOT EXISTS p_business_team_list PARTITION OF business_team FOR VALUES IN (bus_id);
-- All new user need double opt in check up using de acc_hash

CREATE TABLE IF NOT EXISTS game_team(
   id        SERIAL
,  member_id INTEGER  NOT NULL
,  game_id   INTEGER  NOT NULL
,  level     SMALLINT NOT NULL --  0 = master / 1 = tutor 
,  sig       UUID     NOT NULL

,  CONSTRAINT fk_member FOREIGN KEY (member_id) REFERENCES users(id)
,  CONSTRAINT fk_game   FOREIGN KEY (game_id)   REFERENCES games(id)
) INHERITS (log_modification, cron);




-- Avatar Type 
   -- -2 = The Ripper | -1 = GameMaster |  0 = Master  |   1 = Tutor  |  2 = Apprendice
CREATE TABLE IF NOT EXISTS types(
   id         SMALLSERIAL  PRIMARY KEY

,  creator_id SMALLINT     NOT NULL
,  type_flag  SMALLINT     NOT NULL
,  type_name  VARCHAR(25)  NOT NULL
,  about      VARCHAR(250) NOT NULL

,  CONSTRAINT fk_creator FOREIGN KEY (creator_id) REFERENCES user(id)
);


-- Global Avatar

CREATE TABLE IF NOT EXISTS avatars(
   id       SERIAL       NOT     NULL PRIMARY KEY
,  type_id  INTEGER      DEFAULT 2    -- FK
,  link_id  INTEGER      DEFAULT NULL

,  gender   SMALLINT     NOT     NULL -- GENDER: -2 = male |  -1 = female  | 0 = other | 1 ... [feature]  
,  nickname VARCHAR(30)  NOT     NULL
,  sig      VARCHAR(64)  NOT     NULL
,  about    VARCHAR(250)
   
,  UNIQUE(nicknamem, sig),
,  CONSTRAINT fk_type  FOREIGN KEY (type_id) REFERENCES types(id) 
,  CONSTRAINT fk_link  FOREIGN KEY (link_id) REFERENCES avatars(id)
)  INHERITS (log_modification);


-- Specific Student | Aprrendice Avatar because this user can buy avatar's assets

CREATE TABLE IF NOT EXISTS apprendice.avatars(  
   gold   INTEGER DEFAULT 0
,  silver INTEGER DEFAULT 0
,  wallet UUID    DEFAULT uuid_generate_v4 () 
) INHERITS (avatar);


-- Game series chain

CREATE TABLE IF NOT EXISTS series(
   id    SERIAL       PRIMARY KEY 
,  games SMALLINT     DEFAULT 0 
,  title VARCHAR(100) NOT     NULL  
)




-- CRON SECTION

-- You need create two cron job, the fisrt use exec_crontab_ALERT.php
   -- [PARSED_TIMESTAMP_IN_CRON_FORMAT] php exec_crontab_alert.php arg1=table_sig arg2=type[start|end] arg3=flag_alert
   
-- When alert are executed inside de exec_crontab_alert.php create a new cronjob, to start or end a game.
   -- [PARSED_TIMESTAMP_IN_CRON_FORMATED] php exec_crontab.php arg1=table_sig arg2=type[start|end] arg3=flag_alert 

CREATE TABLESPACE ts_cron_partitions
OWNER CURRENT_USER
LOCATION 'lib/postgresql/11/main/mk_appdata/ts_cron_partition';


CREATE TABLE IF NOT EXISTS cron(
   idx SERIAL PRIMARY KEY
,  sig        UUID   DEFAULT uuid_generate_v4 () 
,  table_name VARCHAR(50)  
);

-- The rows for each job || START and END
CREATE TABLE IF NOT EXISTS cronjobs(
   sig      UUID        DEFAULT  uuid_generate_v4 () 
,  t_name   VARCHAR(50) NOT      ''
,  t_idx    INTEGER     NOT      NULL
,  sections SMALLINT    DEFAULT  0           -- SECTIONS COMMENT BELOW   
,  status   SMALLINT    DEFAULT -1           -- STATUS COMMENT BELOY
,  event    TIMESTAMP   WITH    TIME ZONE    -- start date
,  type     BOOLEAN                        -- TRUE = start - FALSE = end 

/*
SECTIONS
FLAG | DEFINE | ABOUT
   0 = daily  = Daily Cron Set up                              -- cron_daily.php
   1 = week   = Less than a Week                               -- cron_week.php
   2 = 2week  = More than a Week and less than a 2 Weeks       -- cron_2week.php
   3 = month  = More than a 2 Weeks and less than a Month      -- cron_month.php
   4 = months = More than a Month                              -- cron_m_month.php
*/
     

/*
STATUS    -- cron_alert.php  -- cron_start_job.php -- cron_close_job.php 
  -4 = waiting= wainting cronjab setting
  -3 = outjob = Cron is out of a job - deadline
  -2 = injob  = Cron job has been set up
  -1 = nojob  = Do not have any cron setting for this asset
   0 = new    = Daily Cron still create create a job
   1 = alter  = If Cron new or Created and need do modified crontab
                for this case alter timestamp and status
   2 = remove = Else if Cron are new, created or altered and now need remove
                for this case set null to timestamp, and set status as -1
*/
) PARTITION by LIST(sections);

CREATE INDEX IF NOT EXISTS idx_section ON cronjobs (sections) TABLESPACE pg_cron_partitions;

CREATE TABLE IF NOT EXISTS p_out                    PARTITION OF cronjobs FOR VALUES IN (-1);
CREATE TABLE IF NOT EXISTS p_cron_daily             PARTITION OF cronjobs FOR VALUES IN (0);
CREATE TABLE IF NOT EXISTS p_cron_more_than_a_week  PARTITION OF cronjobs FOR VALUES IN (1);
CREATE TABLE IF NOT EXISTS p_cron_more_than_2_weeks PARTITION OF cronjobs FOR VALUES IN (2);
CREATE TABLE IF NOT EXISTS p_cron_more_than_a_month PARTITION OF cronjobs FOR VALUES IN (3);

ALTER TABLE cronjobs                 SET TABLESPACE pg_cron_partitions;
ALTER TABLE p_cron_daily             SET TABLESPACE pg_cron_partitions;
ALTER TABLE p_cron_less_than_a_week  SET TABLESPACE pg_cron_partitions;
ALTER TABLE p_cron_more_than_a_week  SET TABLESPACE pg_cron_partitions;
ALTER TABLE p_cron_less_than_a_month SET TABLESPACE pg_cron_partitions;
ALTER TABLE p_cron_more_than_a_month SET TABLESPACE pg_cron_partitions;

CREATE OR REPLACE FUNCTION conn_cron_sig()
RETURNS trigger
LANGUAGE plpgsql
AS $tri$
   BEGIN
      INSERT INTO cronjobs (sig)
      VALUES (new.sig);
      RETURN NEW;
   END;
$tri$;

	
CREATE TRIGGER cron_trigger
AFTER INSERT ON cron
FOR EACH ROW
EXECUTE PROCEDURE conn_cron_sig();



-- procedures
-- create_new_job('table_name', 'row_idx|UUID', status, start, end)

-- In cron instalation api instalation will exist 2 opt, dev and dep
-- The dev option are define for stored procedures with inside entry check-in
-- in this case offer a opportunity to analisy the code before deploy the software.

-- The dep is used by deployed softwares to optime validation, but, use it ONLY
-- at are own risk, because remove ALL verbose, flags and this can cause data.

CREATE OR REPLACE FUNCTION new_cronjob
(                            
   ext_t_name   VARCHAR(50)
,  ext_t_idx    INTEGER
,  ext_status   SMALLINT 
,  ext_sections SMALLINT
,  ext_event    TIMESTAMP WITH TIME ZONE
,  ext_type     BOOLEAN
)
RETURNS SETOF RECORD 
LANGUAGE plpgsql
AS $new_cronjob$
DECLARE
   table_check     BOOLEAN;
   table_idx_check BOOLEAN;
   status_check    BOOLEAN;
   sections_check  BOOLEAN;
   event_check     BOOLEAN;
  
BEGIN  
   table_idx_check := ((ext_t_idx    IS NOT NULL) AND (ext_t_idx <> 0));
   table_check     := ((ext_t_name   IS NOT NULL) AND ( ext_t_name NOT LIKE '') AND (LENGTH(ext_t_name) <= 50));
   status_check    := ((ext_status   IS NOT NULL) OR ((ext_status   >=  -4 )    AND (ext_status         <= 2)));
   sections_check  := ((ext_sections IS NOT NULL) OR ((ext_sections >=   0 )    AND (ext_sections       <= 4)));
   event_check     := ((ext_event    IS NOT NULL) OR  (ext_event > (CURRENT_TIMESTAMP + '1 hour')));
   
   IF ( table_idx_check AND table_check AND status_check AND sections_check AND event_check ) 
   THEN
      -- if table name AND table index exists you can't create a new cronjob row
      IF (
         SELECT EXISTS
            (
            SELECT 1 FROM cronjobs WHERE t_idx = ext_t_idx AND t_name LIKE ext_t_name AND type = ext_type
            )
         ) = FALSE
      THEN
        -- Valid arguments terminal message
        RAISE NOTICE 'All argument are Validated';    

        -- Insert a new row expecific for unique user        
        INSERT INTO cronjobs
           (t_name,     t_idx,     sections,     status,     event,     type)
        VALUES
           (ext_t_name, ext_t_idx, ext_sections, ext_status, ext_event, ext_type);
          
          
      -- If exists a row for this table/id just update row
      ELSE
         UPDATE cronjobs SET
         ,  sections = ext_sections
         ,  status   = ext_status
         ,  event    = ext_event
         WHERE type = ext_type;
           
      END IF;
      
      -- Thases returned parameters will be received in PHP case need create a 
      -- cron jobs in crontab immediatelly
      RETURN QUERY
         SELECT 
            t_uuid as idx
         ,  event  as date
         ,  CASE
               WHEN status = 2 AND sections = -1 THEN 'r'
               WHEN status = 0 AND sections = -1 THEN 'c'
            END AS flags 
         FROM   cronjobs
         WHERE  status = 2    OR 
                sections = -1 AND 
                ( t_name = ext_t_name AND t_idx = ext_t_idx ) ;
      
   ELSE 
      -- Out the warning terminal message and end
      RAISE WARNING 'Invalid Argument';
      
   END IF;
   COMMIT;
   RETURN;
END;
$new_cronjob$;



-- check_daily();
CREATE OR REPLACE PROCEDURE check_daily()
LANGUAGE plpgsql
AS $proc$
BEGIN
   UPDATE cronjibs AS cj SET
   sections =
   CASE 
      WHEN (sections = 0) OR (sections = 1) THEN
         CASE 
            WHEN (status = 0) OR (status = 1) THEN 
               CASE 
                  WHEN ( cj.dt <  (CURRENT_TIMESTAMP + '168 h')) THEN -1
                  WHEN ((cj.dt >= (CURRENT_TIMESTAMP + '168 h')) AND (cj.dt > (CURRENT_TIMESTAMP + '336 h' ))) THEN 2
                  WHEN ((cj.dt >= (CURRENT_TIMESTAMP + '336 h')) AND (cj.dt > (CURRENT_TIMESTAMP + '652 h' ))) THEN 3
                  WHEN ( cj.dt >= (CURRENT_TIMESTAMP + '652 h' ) THEN 4
               END
            WHEN status = 2 THEN
               -1
         END       
   END
   ,
   status =
   CASE
      when (sections = 0) OR (sections = 1) THEN
         CASE 
            WHEN (status = 0) OR (status < 1)  THEN 
               CASE 
                  WHEN (cj.dt <  (CURRENT_TIMESTAMP + '168 h')) THEN  0
                  WHEN (cj.dt >= (CURRENT_TIMESTAMP + '168 h')) THEN -4
               END
            WHEN status = 2 THEN
               -1
         END       
   END
   WHERE (sections = 0) OR (sections = 1) AND ((status > 0) AND (status < 2));

   
   IF (SELECT )

END;
$proc$;
/*
STATUS    -- cron_alert.php  -- cron_start_job.php -- cron_close_job.php 
  -4 = waiting= wainting cronjab setting
  -3 = outjob = Cron is out of a job - deadline
  -2 = injob  = Cron job has been set up
  -1 = nojob  = Do not have any cron setting for this asset
   0 = new    = Daily Cron still create create a job
   1 = alter  = If Cron new or Created and need do modified crontab
                for this case alter timestamp and status
   2 = remove = Else if Cron are new, created or altered and now need remove
                for this case set null to timestamp, and set status as -1
*/








-- check_weekly();
-- check_2week();
-- check_monthly();

-- alter_stack();   -- trigger ALTER
-- remove_stack();  



-- CRON SECTION

--  /\ conferido



('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' IS NOT NULL) OR ( 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' NOT LIKE '') OR (LENGTH('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa') <= 50)       



  -- Precisa testar

CREATE TABLE IF NOT EXISTS games(
   id            SERIAL       PRIMARY KEY,
,  sig           UUID         DEFAULT uuid_generate_v4 ()
,  bus_id        INTEGER      NOT     NULL                     -- FK 
,  medal_id      INTEGER      DEFAULT NULL                     -- FK 
,  serie_id      INTEGER      DEFAULT NULL                     -- CHAIN
,  prev_id       INTEGER      DEFAULT NULL                     -- LINKED LIST 
,  next_id       INTEGER      DEFAULT NULL                     -- LINKED LIST 

,  stories       SMALLINT     DEFAULT 0                        -- modules lenght
,  gamers_cap    SMALLINT     NOT     NULL                     -- apprendice max capacity 
,  title         VARCHAR(80)  NOT     NULL CHECK (name <> '')  -- course name - to exomarketing  
,  guild_name    VARCHAR(80)  NOT     NULL CHECK (name <> '')  -- class  name - to endomarketing 
,  abstract      VARCHAR(100) 
,  about         VARCHAR(500)                                  -- description about the game course 

,  CONSTRAINT fk_business FOREIGN KEY (bus_id)   REFERENCES business(id)
,  CONSTRAINT fk_medal    FOREIGN KEY (medal_id) REFERENCES medals(id)
,  CONSTRAINT fk_serie    FOREIGN KEY (serie_id) REFERENCES series(id)
,  CONSTRAINT fk_prev     FOREIGN KEY (prev_id)  REFERENCES games(id)
,  CONSTRAINT fk_next     FOREIGN KEY (next_id)  REFERENCES games(id)
)  INHERITS (log_modification, cron);

CREATE INDEX game_prev_id ON games (prev_id);
CREATE INDEX game_next_id ON games (next_id);


CREATE TABLE IF NOT EXISTS stories(
   id        SERIAL       PRIMARY KEK
,  course_id INTEGER      NOT     NULL  -- FK
,  medal_id  INTEGER      DEFAULT NULL  -- FK
,  prev_id   INTEGER      DEFAULT NULL  -- LINKED LIST 
,  next_id   INTEGER      DEFAULT NULL  -- LINKED LIST 

,  stages    SMALLINT     DEFAULT 0
,  title     VARCHAR(80)  NOT     NULL
,  abstract  VARCHAR(100)
,  about     VARCHAR(500)

,  CONSTRAINT fk_medal   FOREIGN KEY (medal_id)   REFERENCES medals(id)
,  CONSTRAINT fk_course  FOREIGN KEY (course_id)  REFERENCES games(id) 
,  CONSTRAINT fk_prev    FOREIGN KEY (prev_id)    REFERENCES stories(id)
,  CONSTRAINT fk_next    FOREIGN KEY (next_id)    REFERENCES stories(id)
)  INHERITS (global.log_modification, cron);

CREATE INDEX stories_prev_id ON stories (prev_id);
CREATE INDEX stories_next_id ON stories (next_id);




 -- \/ nao conferido



/*
  "ID"="1"
  "TURL"="[2 first byte is a Type 2 for origin + hash 256 SHA]" {AA02HASH}
  "AD" = [
      {
       "id" = 1,
       "cdata"=TIMESTAMTZ,
       "ftxt"="text.html",
       "stxt"=512
       "ttxt"=[type symbol that defines the importance of the addendum]
      }
  ]
  
  
 
{
   "id":1,
   "TURL":"AA01A3490AD",
   "OPT":
      [
      "question_01",
      "question_02",
      "question_03",
      "question_04",
      "question_05"
      ],
   "SEIKAI":"question_02",
}
}
  
  
  
*/
CREATE TABLE IF NOT EXISTS stages(
   id        SERIAL       PRIMARY KEK
,  sig       UUID         DEFAULT uuid_generate_v4 () , 
,  story_id  INTEGER      NOT     NULL                         -- CHAIN
,  prev_id   INTEGER      DEFAULT 0                            -- LINKED LIST 
,  next_id   INTEGER      DEFAULT 0                            -- LINKED LIST  

   type      SMALLINT                   -- -2 = boss       | -1 = quest |  0 = tutorial | 1 = map (references) | 2 = guide (text) 
,  level     SMALLINT     DEFAULT 0     -- -2 = super_easy | -1 = easy  |  0 = normal   | 1 = hard             | 2 = extreme
,  title     VARCHAR(80)  NOT     NULL
,  abst      VARCHAR(100) NOT     NULL
,  text      VARCHAR(500) NOT     NULL
,  temp_list JSONB        DEFAULT '{}' 

,  CONSTRAINT fk_story FOREING KEY (story_id) REFERENCES stories(id)
,  CONSTRAINT fk_prev  FOREIGN KEY (prev_id)  REFERENCES stages(id)
,  CONSTRAINT fk_next  FOREIGN KEY (next_id)  REFERENCES stages(id)
) INHERITS (log_modification, cron;

CREATE INDEX stages_prev_idx     ON stages (prev_id);
CREATE INDEX stages_next_idx     ON stages (next_id);


CREATE TABLE IF NOT EXISTS quests(
   silver     SMALLINT  DEFAULT 0
,  conditions BOOLEAN                              -- true is required home work
,  required   

,  CONSTRAINT fk_required  FOREIGN KEY (required)  REFERENCES stages(id)
) INHERITS (stages);

CREATE TABLE IF NOT EXISTS boss(
   gold        SMALLINT DEFAULT 0
,  text_tout   SMALLINT DEFAULT 1800
,  qstn_tout   SMALLINT DEFAULT 90 
,  conditions  SMALLINT DEFAULT 0 -- ([windows_out, console_open, print_s ]CLOSE|ALERT|REDUCTION) = { 00000000 - 00000000 )--{0} init    -- {000} punishment flags   --   {00000}  punishment point
) INHERITS (stages);                                                       





-- Skiill list created by the teachers

CREATE TABLE IF NOT EXISTS teachers.avatar_skills(
   id         SERIAL      PRIMARY KEY
,  course_id  INTEGER     NOT NULL
,  skill_name VARCHAR(30) NOT NULL
,  style      BOOLEAN     -- TRUE = progressive style (RPG) , False = Icon Style (Zelda hearth)

,   CONSTRAINT fk_creator FOREIGN KEY (creator_id) REFERENCES users(id)
);

-- GAME AVATAR APPRENDICE SKILL RELATION

CREATE TABLE IF NOT EXISTS apprendice.avatar_skills(
   id        SERIAL   PRIMARY KEY
,  avatar_id INTEGER  NOT NULL
,  game_id   INTEGER  NOT NULL
,  skill_id  INTEGER  NOT NULL

,  exp_bar   SMALLINT DEFAULT 0
   
,  CONSTRAINT fk_avatar FOREIGN KEY (avatar_id) REFERENCES apprendice.avatars(id)
,  CONSTRAINT fk_game   FOREIGN KEY (games_id)  REFERENCES games(id)
,  CONSTRAINT fk_skill  FOREIGN KEY (skill_id)  REFERENCES users(id)
);












-- criar procedure de acesso ao lista de curso , armazenar id - titulo em arvore no navegador
-- para salvar no navegador storage.



/*VIEWS*/
CREATE VIEW OR REPLACE VIEW get_teachers_default
  AS
    SELECT id, prefix, gold, silver, name, last
    FROM   global.user   AS us, 
           global.prefix AS pr,
           global.auth   AS au
    WHERE  
      level = TRUE &&
      ( us.auth_id = au.id ) && ( us.prefix_id = pr.id ) 
      
CREATE VIEW OR REPLACE VIEW get_apprendices_default
  AS
    SELECT id, prefix, gold, silver, name, last
    FROM   global.user   AS us, 
           global.prefix AS pr,
           global.auth   AS au
    WHERE  
      level = FALSE &&
      ( us.auth_id = au.id ) && ( us.prefix_id = pr.id ) 


/*FUNCTION*/

CREATE OR REPLACE PROCEDURE auth_teacher(VARCHAR(60))
  LANGUAGE plpgsql
  AS $$
  BEGIN
    SELECT *  
    FROM   get_teacher_default 
    WHERE  email = $1
    LIMIT  1;
    COMMIT
  END; $$;
  
CREATE OR REPLACE PROCEDURE auth_apprendice(VARCHAR(60))
  LANGUAGE plpgsql
  AS $$
  BEGIN
    SELECT *  
    FROM   get_apprendice_default 
    WHERE  email = $1
    LIMIT  1;
    COMMIT
  END; $$;




--number generator for id
CREATE SEQUENCE IF NOT EXISTS main_id_seq;

CREATE TABLE IF NOT EXISTs heranca(
   sobre text
);

CREATE TABLE IF NOT EXISTS main(
   id serial,
   nome TEXT
) INHERITS(heranca) TABLESPACE pg_default;


CREATE INDEX IF NOT EXISTS idx_nome ON main USING hash (
  nome COLLATE pg_catalog."default"
) TABLESPACE pg_default;


CREATE OR REPLACE PROCEDURE game(x TEXT, y TEXT)
LANGUAGE plpgsql
AS $$
BEGIN
	  EXECUTE format('CREATE TABLE IF NOT EXISTS %s (CHECK (nome LIKE ''%s'')) INHERITS (main)', x, y);
   --commit;
END;
$$;

call game('main_lua', 'lua');
INSERT INTO main (nome) values ('lua');

 select * from main;
--select nome from main_talles;
select nome from main_lua;




