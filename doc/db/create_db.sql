/*COMANDOS DE TESTE*/
SHOW TIMEZONE;

/*Tabela de registro de dados fisicos dos professores e alunos*/
CREATE TABLE doc(
	id     integer NOT NULL CONSTRAINT rf_id PRIMARY KEY UNIQUE,
	name   varchar(40) NOT NULL,  
	lname  varchar(40) NOT NULL,
	rg     varchar(10) NOT NULL UNIQUE,
	cpf    varchar(15) NOT NULL UNIQUE,
	birth  date        
);

CREATE TABLE skill(
    id   integer NOT NULL CONSTRAINT skill_id PRIMARY KEY,
	name varchar(25) NOT NULL
);

CREATE TABLE skill_instructor(
    id integer NOT NULL CONSTRAINT si_id PRIMARY KEY,
	FOREIGN KEY (id) REFERENCES skill(skill_id) NOT NULL,
	FOREIGN KEY (id) REFERENCES instructor(instructor_id) NOT NULL
)

/*Tabela de Professores*/
CREATE TABLE instructor(
	id    integer NOT NULL CONSTRAINT prof_id PRIMARY KEY,
	id_rf integer NOT NULL,
	entry   TIMESTAMPTZ NOT NULL
	FOREIGN KEY (id) REFERENCES doc   (doc_id);
	FOREIGN KEY (id) REFERENCES skill (skill_id);
	FOREIGN KEY (id) REFERENCES login (login_id);

)

CREATE TABLE acess(
    id          integer     NOT NULL CONSTRAINT acess_id PRIMARY KEY,
	acess_ini TIMESTAMPTZ NOT NULL,
	acess_end TIMESTAMPTZ
)

/*Tabela Aluno*/
CREATE TABLE student(
    id integer NOT NULL CONSTRAINT aluno_id PRIMARY KEY,
	entry   TIMESTAMPTZ NOT NULL
	FOREIGN KEY (id) REFERENCES login (login_id);
	FOREIGN KEY (id) REFERENCES doc   (doc_id);
)