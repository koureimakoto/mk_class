<?php
require_once 'global_config.php';
require_once __COMMONS_ERRORS__;
require_once __SESSION__;
require_once __USER__;

//INDEX.PHP
$session   = new TutorSession();
$user      = new TutorUser();

$render    = new RenderRequests(new TutorErrorRequest());
$procedural= new ProceduresRequests();

if($session->status())
   {
   $render->panel();
   }
else if($rqst->Filter->post())
   {
   if($rqst->auth($this->User))
      $render->Error->auth($rqst->data());
      
   $render->auth($rqst->data());   
   }
   
$render->index();

?>
