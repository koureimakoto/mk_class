<?php
require_once 'global_config.php';
require_once __COMMONS_ERRORS__;
require_once __TUTOR_SESSION__;
require_once __TUTOR_USER__;

//INDEX.PHP
$user      = new TutorUser();
$session   = new TutorSession();

$render    = new RenderRequests(new TutorErrorRequest());
$procedures= new ProceduresRequests($session, $user);

if(!$session->status())
   {
   $render->index();
   }
else
   {
   // ->data() : $this obj
   $rslt;
   if($procedural->Filter->post())
      {       
      switch($procedures->path())
         {
         case 'create_courses':
            {
            if(!(($rslt = $procedures->Create->courses()) == NO_PERMISSION))
               $render->Error->Create->courses();

            $render->data($rslt)->Create->courses();
            }
         }
     }
   else
      {
      if($procedural->Filter->get()) 
         {
         switch($procedures->path())
            {
            case 'courses':
               {
               if(!$rslt = $procedures->courses())
                  $render->Error->courses();

               $render->data($rslt)->courses();
               }
            case 'modules':
               {
               if(!$rslt = $procedures->modules())
                  $render->Error->modules();
   
               $render->data($procedures->data()->modules();
               }
            case 'lesson':
               {
               if(!$rslt = $procedures->lesson())
                  $render->Error->lesson();

               $render->data($rslt))->lesson();
               }
            }  
         }
      }
   
$render->index();

?>
