<?php 

/******************************************************************************/
class Filter
{
   public function
   check($arr) : bool
      {
      $regex = "(\s|=|;|'|\")"; 
      foreach($arr as $key=>$value )
         {
         $match = preg_match($regex, $value, $matches);
         if($match != 0) 
            return(FALSE);
         }
      return(TRUE);
      }
 

   //CHECK REQUESTS

   public function
   get()  : bool
      {
      $this->is_get = 
      return($this->is_get = (($_SERVER['REQUEST_METHOD'] != 'POST') &&
                              (!empty($_SERVER['PHP_SELF'])        ) &&
                              (isset($_GET['p'])                   ) &&
                              (self::check($_GET))                ));
      }
   
   
   public function
   post() : bool
      {
      return($this->is_post =(($_SERVER['REQUEST_METHOD'] == 'POST'    ) && 
                              (!empty($_SERVER['PHP_SELF'])            ) &&
                              (isset($_GET['p'])                       ) && 
                              (self::check($_GET) && self::check($_POST))));
      }
   
   
   //PREPARAR INNER RESPONSE PROTOCOL

   private function
   prepareResponse()
      {
      $arg_list = func_get_args();
      $arg_num  = func_num_args();
      for($i = 0; $i < $arg_num; $i++)
         $this->data_response += $arg_list[$i];    
      }
}
?>
