<?php 
require_once('session.abstract.php');

class TSession extends MKASession
{
   protected function
   setSession($data, $new_session)
   {
      $_SESSION['N_SESSION' ] = $new_session; 
      $_SESSION['HASH'      ] = self::sessionHash();
      $_SESSION['ID'        ] = $data['id'];
      $_SESSION['TUTOR'     ] = $data['name'];
      $_SESSION['PREFIX'    ] = $data['prefix'];
      $_SESSION['LEVEL'     ] = $data['level'];
   }
}

?>
