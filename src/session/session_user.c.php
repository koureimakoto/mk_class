<?php 
require_once('session.abstract.php');

class USession extends MKASession
{
   protected $key    = "cafc09457ff434c4ca0b1db2c0f580508c2202adb2e29d22e481764b139992f7";
 
    protected function
   sessionHash() : string
   {
      $hash = "{$_POST['ulogin']}{$_SERVER['HTTP_USER_AGENT']}{$this->key}";
      $this->session_hash = $this->key.hash("sha256",'oi'); 
      return($this->session_hash);
   }
   
   protected function
   setSession($data, $new_session)
   {
      $_SESSION['N_SESSION'] = $new_session; 
      $_SESSION['HASH'     ] = self::sessionHash();
      $_SESSION['ID'       ] = $data['uid'];
      $_SESSION['TUTOR'    ] = $data['uname'];
      $_SESSION['PREFIX'   ] = $data['prefix'];
   }
}


?>
