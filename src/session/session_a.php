<?php 

abstract class MKASession
{
   protected $salt = "60ec5646679fd485d182081f47ab6716798a3cd22a76c7e7678e05519ae1dde2",
             $read_only,
             $hash,
             $s_data = 
                [
                'start'  => '',
                'status' => ''
                ];


   public function
   __construct($read_only = TRUE)
      {
      $this->read_only = $read_only;
      ini_set('session.use_strict_mode', 1);
      if(session_status() !== PHP_SESSION_ACTIVE)
         {
         self::sessionStart($this->read_only);
         if($this->s_data['start'])
            {
            if(self::chkHash())
               $this->s_data['status'] = TRUE;
            else
               $this->s_data['status'] = FALSE;
            }
         }
      }
      
   public function
   sessionStart()
      {
      $session = session_start
         ([
         'cookie_lifetime' => 86400,
         'read_and_close'  => $this->read_only
         ]);
         
      if(!$session)
         $this->s_data['start'] = FALSE;
      else
         $this->s_data['start'] = TRUE;
      }
   
   public function
   createSession($data, string $session_id_name)
      {      
      if(!$this->read_only)
         {
         self::removeSession();
         $new_session = session_create_id($session_id_name);
         ini_set('session.use_strict_mode', 0);
         session_id($new_session);    
         
         self::sessionStart();
         
         $this::setSession($data, $new_session);
         session_commit();
         }
      }
           
   public function
   removeSession()
      {
      session_unset();
      session_destroy();
      }

   public function
   updateSession($old_session)
      {
      session_regenerate_id();
      }
      
   
   
      
   protected function
   sessionHash() : string
      {
      $hash = "{$_SERVER['HTTP_USER_AGENT']}{$this->salt}";
      $this->session_hash = $this->salt.hash("sha256",$hash); 
      return($this->session_hash);
      } 
      
   public function
   chkHash()
      {
      return((isset($_SESSION['HASH'])     &&
             (!empty($_SESSION['HASH'])))  &&
             ($this::getHash() === $this::sessionHash()));
      }
      
   public function
   getHash()
      {
      return($this->hash);
      }




   public function
   getSession()
      {
      return(['session_data'=>$this->s_data]);
      }   

   public function
   getStatus()
      {
      return($this->s_data['status']);
      }





   abstract protected function
   setSession($data, $new_session);
}
?>
