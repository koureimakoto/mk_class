<?php
namespace DB;

abstract class MKA_Database
{
   protected $conn_str;
   protected $conn;
   protected $rslt;
   
   protected $db_server;
   protected $db_port;
   protected $db_user;
   protected $db_passwd;
   
   protected $db_erro = DB_NO_ERRORS;
   
   abstract public function
   getSign();
      
   public function
   getLastError()
      {
      return($this->db_error);
      }
     
   protected function
   setError($error_const)
      {
      $this->db_error = $error_const;
      }
}
?>
