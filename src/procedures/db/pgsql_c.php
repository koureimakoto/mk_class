<?php

namespace DB;
require_once "connection_conf.php";
require_once "db_a.php";
require_once "db_i.php";
use DB\MKA_Database as DBA;
use DB\MKI_Database as DBI;

class MK_PGsql extends MKA_Database implements MKI_Database
{
   protected $conn_str = PGHOST . ' '. PGPORT.' '. ' '. PGUSER. ' '. PGPSWD;

   public function 
   __construct() 
      {
      $this->conn = pg_connect($this->conn_str);
      if( pg_connection_status($this->conn) === PGSQL_CONNECTION_BAD )
         $this->db_erro = DB_CONNECTION_ERROR;
      }

   public function
   getSign()
      {
      return(['sign'=>'PGSQL']);
      }
   
   //CLOSE CONNECTION

   public function
   close()
      {
      pg_close(self::getDataBase());
      }
      
      
   //REQUEST MULTIPLES 
         
   public function
   query( $sql )
      {
      $final = [];

      /*Prepare the data result*/
      if( !($this->rslt = pg_query( $this->conn, $sql )) )
         return DB_QUERY_ALL_ERROR;
      else
         while( $row = pg_fetch_assoc($this->rslt) )
            $final[] = $row;     
      
      return $final;
      }
  
   
   //REQUEST ONE RESULT IN ARRAY
   
   public function
   queryOne($sql)
      {  
      if(!$this->rslt = pg_query( $this->conn, $sql ))
         return DB_QUERY_ONE_ERROR;
  
      return(pg_fetch_assoc($this->rslt)); 
      }
}


?>
