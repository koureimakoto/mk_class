<?php
namespace DB;

interface MKI_Database
{     
   public function 
   __construct();
   
   public function
   close();
       
   public function
   query($sql);
  
   public function
   queryOne($sql);
}

?>
