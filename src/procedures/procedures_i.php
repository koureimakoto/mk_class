<?php

interface MKI_Procedures
{
   public function
   insert($db);
  
   public function
   select($db);
  
   public function
   remove($db);
  
   public function
   update($db);
}
?>
