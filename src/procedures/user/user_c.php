<?php
require_once __DIR__."/user_procedures_a.php";

class MK_User extends MKA_UserProcedures
{
   protected $u_data;

   public function
   createPasswd($passwd_txt)
      {
      $passwd_hash = password_hash
        (
        $passwd_txt,
        PASSWORD_ARGON2ID,
        ['memory_cost'=>PASSWORD_ARGON2_DEFAULT_MEMORY_COST,
         'time_cost'  =>PASSWORD_ARGON2_DEFAULT_TIME_COST,
         'threads'    =>'2']
        ); 
       return($passwd_hash);    
      }
   
   public function
   checkPassword($passwd_txt, $passwd_hash)
      {
      return(password_verify($passwd_txt, $passwd_hash));
      }
      

   public function
   checkEmail($email)
      {
      return(filter_var($email, FILTER_VALIDATE_EMAIL));
      }
   

   public function
   setUser($arr)
      {
      foreach( $arr as $key=>$value )
         $this->u_data['user'][$key] = $value;
         
         var_dump($this->u_data);
      }
   
   public function
   getUser()
      {
      return($this->u_data);
      }
}
?>
